package com.example.test.dto;

import lombok.Data;

@Data
public class ContentDto {
    private String id;
    private String text;
    private int likes;
}
