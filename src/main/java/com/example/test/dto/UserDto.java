package com.example.test.dto;

import lombok.Data;

@Data
public class UserDto {
    private String username;
    private String email;
}