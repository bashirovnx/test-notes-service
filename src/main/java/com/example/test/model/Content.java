package com.example.test.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contents")
@Data
public class Content {
    @Id
    private String id;
    private String text;
    private int likes;
}
