package com.example.test.controller;

import com.example.test.dto.ContentDto;
import com.example.test.service.ContentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/content")
@RequiredArgsConstructor
public class ContentController {

  private final ContentService contentService;

  @GetMapping
  public ResponseEntity<List<ContentDto>> getAllContents() {
    List<ContentDto> contentDtos = contentService.getAllContents();
    return ResponseEntity.ok(contentDtos);
  }

  @PostMapping
  public ResponseEntity<ContentDto> createContent(@RequestBody ContentDto contentDto) {
    ContentDto createdContentDto = contentService.createContent(contentDto);
    return ResponseEntity.ok(createdContentDto);
  }

  @PutMapping("/{id}")
  public ResponseEntity<ContentDto> updateContent(
      @PathVariable("id") String id, @RequestBody ContentDto contentDto) {
    ContentDto updatedContentDto = contentService.updateContent(id, contentDto);
    return ResponseEntity.ok(updatedContentDto);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteContent(@PathVariable("id") String id) {
    contentService.deleteContent(id);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("/{id}/like")
  public ResponseEntity<Void> likeContent(@PathVariable("id") String id) {
    contentService.likeContent(id);
    return ResponseEntity.noContent().build();
  }

  @PostMapping("/{id}/unlike")
  public ResponseEntity<Void> unlikeContent(@PathVariable("id") String id) {
    contentService.unlikeContent(id);
    return ResponseEntity.noContent().build();
  }
}
