package com.example.test.service;

import com.example.test.dto.ContentDto;
import com.example.test.mapper.ContentMapper;
import com.example.test.model.Content;
import com.example.test.repository.ContentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContentService {
  private ContentRepository contentRepository;

  private ContentMapper contentMapper;

  public List<ContentDto> getAllContents() {
    List<Content> contents = contentRepository.findAll();
    return contents.stream().map(contentMapper::toDto).collect(Collectors.toList());
  }

  public ContentDto createContent(ContentDto contentDto) {
    Content content = new Content();
    content.setText(contentDto.getText());
    content.setLikes(contentDto.getLikes());
    Content savedContent = contentRepository.save(content);
    return contentMapper.toDto(savedContent);
  }

  public ContentDto updateContent(String id, ContentDto contentDto) {
    Content content =
        contentRepository.findById(id).orElseThrow(() -> new RuntimeException("Content not found"));
    content.setText(contentDto.getText());
    content.setLikes(contentDto.getLikes());
    Content savedContent = contentRepository.save(content);
    return contentMapper.toDto(savedContent);
  }

  public void deleteContent(String id) {
    contentRepository.deleteById(id);
  }

  public ContentDto likeContent(String id) {
    Content content =
        contentRepository.findById(id).orElseThrow(() -> new RuntimeException("Content not found"));
    content.setLikes(content.getLikes() + 1);
    Content savedContent = contentRepository.save(content);
    return contentMapper.toDto(savedContent);
  }

  public ContentDto unlikeContent(String id) {
    Content content =
        contentRepository.findById(id).orElseThrow(() -> new RuntimeException("Content not found"));
    content.setLikes(content.getLikes() - 1);
    Content savedContent = contentRepository.save(content);
    return contentMapper.toDto(savedContent);
  }
}
