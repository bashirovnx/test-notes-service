package com.example.test.service;

import com.example.test.dto.UserDto;
import com.example.test.exception.ResourceNotFoundException;
import com.example.test.mapper.UserMapper;
import com.example.test.model.User;
import com.example.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

  private final UserRepository userRepository;
  private final UserMapper userMapper;

  @Autowired
  public UserService(UserRepository userRepository, UserMapper userMapper) {
    this.userRepository = userRepository;
    this.userMapper = userMapper;
  }

  public List<UserDto> getAllUsers() {
    List<User> users = userRepository.findAll();
    return userMapper.toDtoList(users);
  }

  public UserDto getUserById(String id) {
    Optional<User> user = userRepository.findById(id);
    if (user.isPresent()) {
      return userMapper.toDto(user.get());
    } else {
      throw new ResourceNotFoundException("User with ID " + id + " not found");
    }
  }

  public UserDto createUser(UserDto userDto) {
    User user = userMapper.toModel(userDto);

    user = userRepository.save(user);

    return userMapper.toDto(user);
  }

  public UserDto updateUser(String id, UserDto userDto) {
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isPresent()) {
      User user = optionalUser.get();
      //      user.setFirstName(userDto.getFirstName());
      //      user.setLastName(userDto.getLastName());
      user.setEmail(userDto.getEmail());
      //      user.setPassword(userDto.getPassword());
      user = userRepository.save(user);
      return userMapper.toDto(user);
    } else {
      throw new ResourceNotFoundException("User with ID " + id + " not found");
    }
  }

  public void deleteUser(String id) {
    Optional<User> optionalUser = userRepository.findById(id);
    if (optionalUser.isPresent()) {
      userRepository.delete(optionalUser.get());
    } else {
      throw new ResourceNotFoundException("User with ID " + id + " not found");
    }
  }
}
