package com.example.test.mapper;

import com.example.test.dto.UserDto;
import com.example.test.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto toDto(User user);

    List<UserDto> toDtoList(List<User> users);

    User toModel(UserDto userDto);
}