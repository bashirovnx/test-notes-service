package com.example.test.mapper;

import com.example.test.dto.ContentDto;
import com.example.test.model.Content;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ContentMapper {

    @Mapping(target = "id", ignore = true)
    Content toModel(ContentDto contentDto);

    ContentDto toDto(Content content);

    List<ContentDto> toDtoList(List<Content> contents);

}
